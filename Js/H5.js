function isWeixinFun() {
	var ua = window.navigator.userAgent.toLowerCase();
	console.log(ua); //mozilla/5.0 (iphone; cpu iphone os 9_1 like mac os x) applewebkit/601.1.46 (khtml, like gecko)version/9.0 mobile/13b143 safari/601.1
	if (ua.match(/MicroMessenger/i) == 'micromessenger') {
		return true;
	} else {
		return false;
	}
}

if (isWeixinFun()) {
	//在微信中打开
	var strVar = "";
	strVar += '<img src="img/downloadWithSafari.jpg" class="coverPic" />';
	$(".wrapall").append(strVar);
}

getVersion();

function getVersion() {
	// toast.show({
	// 	text: '正在加载',
	// 	loading: true
	// });
	var url = 'http://116.236.149.165:8090/SubstationWEBV2/sys/listAppVersion';
	$.ajax({
		type: 'GET',
		url: url,
		data: "",
		// beforeSend: function (request) {
		// 	request.setRequestHeader("Authorization", tokenFromAPP)
		// },
		success: function (result) {
			// toast.hide();
			if (result == undefined) {
				// $.toast("信息错误");
				return;
			} else {
				if (result.code == "200") {
					var versionList = result.data.list;
					if (versionList == undefined) {
						// $.toast("信息错误");
						return;
					}
					$.each(versionList, function (key, val) {
						if (isAndroidFun() == 'Android' && val.fId == "ab55ce55Ac213hlkhl23419f179c5f6f") {
							// alert("在安卓中打开");
							var strVar = "";
							strVar += ' <a href= ' + val.fAppurl + '';
							strVar += "            class=\"downbtn\">安卓下载<\/a>";
							strVar += "        <br>";
							strVar += "        <p class=\"bbp1\">";
							strVar += "            <span>版本号<\/span>：Android " + val.fVersion + "";
							strVar += "        <\/p>";
							strVar += " <p class=\"bbp2\">发布日期：" + val.fUpdatetime + "<\/p>";
							$(".wrapall").append(strVar);
						} else if (isAndroidFun() == 'IOS' && val.fId == "iose70eeb320a58230925c02e7") {
							// alert("在苹果中打开");
							var strVar = "";
							strVar += ' <a href= ' + val.fAppurl + '';
							strVar += "            class=\"downbtn\">iphone下载<\/a>";
							strVar += "        <br>";
							strVar += "        <p class=\"bbp1\">";
							strVar += "            <span>版本号<\/span>：iOS " + val.fVersion + "";
							strVar += "        <\/p>";
							strVar += " <p class=\"bbp2\">发布日期：" + val.fUpdatetime + "<\/p>";
							$(".wrapall").append(strVar);
						}
					});

				} else {
					// Substation.showCodeTips("zh", data.code);
				}
			}
		},
		error: function () {
			// toast.show({
			// 	text: '数据请求失败',
			// 	duration: 2000
			// });
		}
	})
}

//判断使用的手机是android还是IOS
function isAndroidFun() {
	var u = window.navigator.userAgent,
		app = window.navigator.appVersion;
	var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1;
	var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
	if (isAndroid) {
		return "Android";
	}
	if (isIOS) {
		return "IOS";
	}
}

(function (doc, win) {
	var docEl = doc.documentElement,
		resizeEvt = 'orientationchange' in window ? 'orientationchange' :
		'resize',
		recalc = function () {
			var clientWidth = docEl.clientWidth;
			if (!clientWidth)
				return;
			docEl.style.fontSize = (clientWidth / 7.5) + 'px';
		};
	if (!doc.addEventListener)
		return;
	win.addEventListener(resizeEvt, recalc, false);
	doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);